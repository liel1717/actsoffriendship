
CREATE TABLE IF NOT EXISTS accounts (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	username VARCHAR(50) NOT NULL,
	password TEXT NOT NULL,
	CONSTRAINT usernameUnique UNIQUE (username)
);

CREATE TABLE IF NOT EXISTS friends (
	id1 INT UNSIGNED,
	id2 INT UNSIGNED,
	PRIMARY KEY(id1, id2),
	FOREIGN KEY(id1) REFERENCES accounts(id),
	FOREIGN KEY(id2) REFERENCES accounts(id)
);

CREATE TABLE IF NOT EXISTS messages (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	senderUsername VARCHAR(50),
	receiverUsername VARCHAR(50),
	message TEXT NOT NULL,
	date DATETIME NOT NULL,
	FOREIGN KEY(senderUsername) REFERENCES accounts(username),
	FOREIGN KEY(receiverUsername) REFERENCES accounts(username)
);

CREATE TABLE IF NOT EXISTS shows (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	accountId INT UNSIGNED,
	name VARCHAR(50) NOT NULL,
	location VARCHAR(50) NOT NULL,
	date TEXT NOT NULL,
	FOREIGN KEY (accountId) REFERENCES accounts(id)
);

INSERT INTO accounts (username, password) VALUES ("eddie", "$2a$08$8QGbsTzPB0Tgz7Uu5PMW9OJGy5LQJNR9XN.09E1eevLsRjulHF7kq"); /*FOR EXAMINER: passwords are abc123*/
INSERT INTO accounts (username, password) VALUES ("alice", "$2a$08$8QGbsTzPB0Tgz7Uu5PMW9OJGy5LQJNR9XN.09E1eevLsRjulHF7kq"); 
INSERT INTO accounts (username, password) VALUES ("elon", "$2a$08$8QGbsTzPB0Tgz7Uu5PMW9OJGy5LQJNR9XN.09E1eevLsRjulHF7kq"); 

INSERT INTO shows (accountId, name, location, date) VALUES (1, "Coldplay", "Roskilde", "2003-06-27" );
INSERT INTO shows (accountId, name, location, date) VALUES (1, "Iron Maiden", "Roskilde", "2003-06-29" );
INSERT INTO shows (accountId, name, location, date) VALUES (1, "Coldplay", "Glastonbury", "2005-06-15" );
INSERT INTO shows (accountId, name, location, date) VALUES (1, "Oasis", "Glastonbury", "2005-06-15" );
INSERT INTO shows (accountId, name, location, date) VALUES (1, "Blur", "York", "2002-01-03" );
INSERT INTO shows (accountId, name, location, date) VALUES (1, "Blur", "London", "2000-07-02" );

INSERT INTO shows (accountId, name, location, date) VALUES (2, "Coldplay", "Roskilde", "2003-06-27" );
INSERT INTO shows (accountId, name, location, date) VALUES (2, "Iron Maiden", "Stockholm", "2008-01-15" );
INSERT INTO shows (accountId, name, location, date) VALUES (2, "Iron Maiden", "New York", "2015-07-01" );
INSERT INTO shows (accountId, name, location, date) VALUES (2, "Oasis", "London", "2004-05-15" );
INSERT INTO shows (accountId, name, location, date) VALUES (2, "Refused", "Ryd", "1999-09-19" );

INSERT INTO shows (accountId, name, location, date) VALUES (3, "Coldplay", "Stockholm", "2011-07-23" );
INSERT INTO shows (accountId, name, location, date) VALUES (3, "Iron Maiden", "Stockholm", "2008-01-15" );
INSERT INTO shows (accountId, name, location, date) VALUES (3, "Coldplay", "Glastonbury", "2005-06-15" );
INSERT INTO shows (accountId, name, location, date) VALUES (3, "Oasis", "Stockholm", "2009-02-16" );
INSERT INTO shows (accountId, name, location, date) VALUES (3, "Refused", "Ryd", "2001-03-29" );
INSERT INTO shows (accountId, name, location, date) VALUES (3, "Eagles", "Los Angeles", "1976-07-07" );
INSERT INTO shows (accountId, name, location, date) VALUES (3, "Eagles", "San Francisco", "1979-06-28" );

