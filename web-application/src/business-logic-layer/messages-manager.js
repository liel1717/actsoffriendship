module.exports = function ({ messagesRepository }) {

    return {

        createMessage: function (accountLoggedIn, receiverUsername, message, callback) {

            if (!accountLoggedIn)
                callback(["notLoggedIn"])
            else if (message.length < 1)
                callback(["no message entered"])
            else
                messagesRepository.createMessage(accountLoggedIn.username, receiverUsername, message, callback)
        },

        getAccountMessages: function (accountLoggedIn, callback) {

            if (accountLoggedIn) {
                messagesRepository.getAccountMessages(accountLoggedIn.username, callback)
            } else
                callback(["notLoggedIn"], null)
        }
    }
}