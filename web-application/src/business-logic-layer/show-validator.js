
module.exports = function ({ }) {

    return {

        getErrorsNewShow: function (name, location, date) {

            const errors = []

            if (name.length < 1)
                errors.push("no band/artist entered")
            if (location.length < 1)
                errors.push("no location entered")
            if (date.length < 1)
                errors.push("no date entered")

            console.log(errors)
            return errors
        }
    }
}