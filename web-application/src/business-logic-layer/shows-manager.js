module.exports = function ({ showsRepository, showValidator }) {

	return {

		getAllShows: function (callback) {
			showsRepository.getAllShows(callback)
		},

		addShow: function (accountLoggedIn, payload, name, location, date, callback) {

			const errors = showValidator.getErrorsNewShow(name, location, date)

			if (errors.length > 0)
				callback(errors)
			else if (accountLoggedIn)
				showsRepository.addShow(accountLoggedIn.id, name, location, date, callback)
			else if (!accountLoggedIn && !payload)
				callback(["notLoggedIn"])
			else if (!payload)
				callback(["invalid_client"])
			else
				showsRepository.addShow(payload.accountId, name, location, date, callback)
		},

		getShowsByAccountId: function (accountLoggedIn, payload, accountId, callback) {
			if (accountLoggedIn)
				showsRepository.getShowsByAccountId(accountLoggedIn.id, callback)
			else if (!accountLoggedIn && !payload)
				callback(["notLoggedIn"])
			else if (!payload || accountId != payload.accountId)
				callback(["invalid_client"])
			else
				showsRepository.getShowsByAccountId(accountId, callback)
		},

		getShowById: function (id, payload, callback) {
			if (!payload)
				callback(["invalid_client"])
			else
				showsRepository.getShowById(id, callback)
		},

		getUniqueShowNamesForAccount: function (accountLoggedIn, callback) {
			if (accountLoggedIn)
				showsRepository.getUniqueShowNamesForAccount(accountLoggedIn.id, callback)
			else
				callback(["notLoggedIn"], null)
		},

		deleteShow: function (id, payload, callback) {

			if (!payload)
				callback(["invalid_client"])
			else
				showsRepository.deleteShow(id, payload.accountId, callback)
		},

		updateShow: function (id, payload, name, location, date, callback) {

			if (!payload)
				callback(["invalid_client"])
			else
				showsRepository.updateShow(id, payload.accountId, name, location, date, callback)
		}
	}
}











