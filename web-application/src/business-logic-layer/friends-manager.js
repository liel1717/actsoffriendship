module.exports = function ({ friendsRepository, accountManager }) {

    return {

        addFriend: function (accountLoggedIn, id1, id2, callback) {

            if (accountLoggedIn)
                friendsRepository.addFriend(id1, id2, callback)
            else
                callback(["notLoggedIn"])
        },

        getFriendsAccounts: function (accountLoggedIn, callback) {

            if (accountLoggedIn) {
                const friendsAccounts = []

                friendsRepository.getFriendsIds(accountLoggedIn.id, function (errors, friendsIds) {

                    if (errors.length > 0)
                        callback(["databaseError"], null)
                    else {
                        for (var i = 0; i < friendsIds.length; i++) {
                            accountManager.getAccountById(friendsIds[i], function (errors, account) {
                                if (errors.length > 0) {
                                    callback(["databaseError"], null)
                                    return
                                }
                                friendsAccounts.push(account)
                            })
                        }
                        callback([], friendsAccounts)
                    }
                })
            } else
                callback(["notLoggedIn"], null)
        }
    }
}