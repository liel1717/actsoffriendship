module.exports = function ({ }) {

	const MIN_USERNAME_LENGTH = 3
	const MAX_USERNAME_LENGTH = 10
	const MIN_PASSWORD_LENGTH = 6
	const MAX_PASSWORD_LENGTH = 30

	return {

		getErrorsNewAccount: function (account) {

			const errors = []

			if (!account.hasOwnProperty("username"))
				errors.push("usernameMissing")
			if (account.username.length < MIN_USERNAME_LENGTH)
				errors.push("usernameTooShort")
			if (MAX_USERNAME_LENGTH < account.username.length)
				errors.push("usernameTooLong")
			if (account.password.length < MIN_PASSWORD_LENGTH)
				errors.push("passwordTooShort")
			if (account.password.length > MAX_PASSWORD_LENGTH)
				errors.push("passwordTooLong")
			if (account.password != account.password2)
				errors.push("passwordsAredifferent")

			return errors
		}
	}
}




