const bcrypt = require('bcryptjs')

module.exports = function ({ accountRepository, accountValidator }) {

	return {

		getAllAccounts: function (callback) {
			accountRepository.getAllAccounts(callback)
		},

		getAccountByUsername: function (username, callback) {
			accountRepository.getAccountByUsername(username, callback)
		},

		getAccountsByBandSeen: function (band, accountId, callback) {
			accountRepository.getAccountsByBandSeen(band, accountId, callback)
		},

		getAccountsByShowSeen: function (show, accountId, callback) {
			accountRepository.getAccountsByShowSeen(show, accountId, callback)
		},

		getAccountById: function (id, callback) {
			accountRepository.getAccountById(id, callback)
		},

		createAccount: function (username, password, password2, callback) {

			const account = { username: username, password, password, password2: password2 }
			const errors = accountValidator.getErrorsNewAccount(account)

			if (0 < errors.length) {
				callback(errors, null)
				return
			}

			var hash = bcrypt.hashSync(password, 8);
			accountRepository.createAccount(username, hash, callback)
		},

		signIn: function (logInInfo, callback) {

			const username = logInInfo.username
			const enteredPassword = logInInfo.password

			accountRepository.getAccountByUsername(username, function (errors, account) {
				if (errors[0] == "databaseError" || account == null)
					callback(['databaseError'], null)
				else {
					bcrypt.compare(enteredPassword, account.password, function (errors, result) {
						if (result)
							callback([], account)
						else
							callback(['loginInfoIncorrect'], null)
					})
				}
			})
		},

		validateLogIn: function (grant_type, username, enteredPassword, callback) {

			if (grant_type != "password")
				callback(["unsupported_grant_type"])
			else
				accountRepository.getAccountByUsername(username, function (errors, account) {
					if (errors.length > 0 || account == null)
						callback(["databaseError"], null)
					else {
						bcrypt.compare(enteredPassword, account.password, function (errors, result) {
							if (result)
								callback([], account)
							else
								callback(["invalid_client"], null)
						})
					}
				})
		}
	}
}












