const db = require('./db')

module.exports = function ({ }) {

    return {

        addFriend: function (id1, id2, callback) {

            const query = 'INSERT INTO friends (id1, id2) VALUES (?, ?)'
            const values = [id1, id2]
            values.sort()

            db.query(query, values, function (error) {

                if (error) {
                    if (error.errno = 1062) {
                        callback(["Already friends :)"])
                    } else {
                        callback(["databaseError"])
                    }
                } else {
                    callback([])
                }
            })
        },

        getFriendsIds: function (id, callback) {

            const query = 'select * from friends where id1 = ? OR id2 = ?'
            const friendsIds = []

            db.query(query, [id, id], function (error, ids) {

                if (error) {
                    callback(["databaseError"], null)
                } else {
                    for (var i = 0; i < ids.length; i++) {
                        if (ids[i].id1 != id)
                            friendsIds.push(ids[i].id1)
                        else
                            friendsIds.push(ids[i].id2)
                    }
                    callback([], friendsIds)
                }
            })
        }
    }
}