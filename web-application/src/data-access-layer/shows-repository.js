const db = require('./db')

module.exports = function ({ }) {

    return {

        getShowsByAccountId: function (accountId, callback) {

            const query = 'SELECT * FROM shows WHERE accountId = ? ORDER BY date'

            db.query(query, [accountId], function (error, shows) {
                if (error) {
                    callback(['databaseError'], null)
                } else {
                    callback([], shows)
                }
            })
        },

        getShowById: function (id, callback) {

            const query = 'SELECT * FROM shows WHERE id = ? LIMIT 1'

            db.query(query, [id], function (error, shows) {
                if (error) {
                    callback(['databaseError'], null)
                } else {
                    callback([], shows[0])
                }
            })
        },

        addShow: function (accountId, name, location, date, callback) {

            const query = 'INSERT INTO shows (accountId, name, location, date) VALUES (?, ?, ?, ?)'
            const values = [accountId, name, location, date]

            db.query(query, values, function (error) {
                if (error) {
                    callback(['databaseError'])
                } else {
                    callback([])
                }
            })
        },

        getUniqueShowNamesForAccount: function (accountId, callback) {

            const query = 'SELECT * FROM shows WHERE accountId = ? ORDER BY date'

            db.query(query, [accountId], function (error, shows) {
                if (error) {
                    callback(['databaseError'], null)
                } else {
                    const uniqueShowsNames = []
                    for (var i = 0; i < shows.length; i++) {
                        if (i == 0)
                            uniqueShowsNames.push(shows[i])
                        for (var j = 0; j < uniqueShowsNames.length; j++) {
                            if (uniqueShowsNames[j].name == shows[i].name)
                                break
                            if (j == uniqueShowsNames.length - 1)
                                uniqueShowsNames.push(shows[i])
                        }
                    }
                    callback([], uniqueShowsNames)
                }
            })
        },

        deleteShow: function (id, accountId, callback) {

            const query = "DELETE FROM shows WHERE id = ? AND accountId = ?"
            const values = [id, accountId]

            db.query(query, values, function (error, results) {
                if (error)
                    callback(['databaseError'])
                else if (results.affectedRows)
                    callback(['deleted'])
                else
                    callback([])
            })
        },

        updateShow: function (id, accountId, name, location, date, callback) {

            const query = "UPDATE shows SET name = ?, location = ?, date = ? WHERE id = ? AND accountId = ?"
            const values = [name, location, date, id, accountId]

            db.query(query, values, function (error, results) {
                if (error)
                    callback(['databaseError'])
                else if (results.affectedRows)
                    callback(['updated'])
                else
                    callback([])
            })
        }
    }
}

