const db = require('./db')

module.exports = function ({ }) {

	return {

		getAllAccounts: function (callback) {

			const query = `SELECT * FROM accounts ORDER BY username`

			db.query(query, function (error, accounts) {
				if (error)
					callback(['databaseError'], null)
				else
					callback([], accounts)
			})
		},

		getAccountByUsername: function (username, callback) {

			const query = `SELECT * FROM accounts WHERE username = ? LIMIT 1`

			db.query(query, [username], function (error, accounts) {
				if (error)
					callback(['databaseError'], null)
				else
					callback([], accounts[0])
			})
		},

		createAccount: function (username, password, callback) {

			const query = `INSERT INTO accounts (username, password) VALUES (?, ?)`
			const values = [username, password]

			db.query(query, values, function (error, results) {
				if (error) {
					if (error.errno == 1062)
						callback(['usernameTaken'], null)
					else
						callback(['databaseError'], null)
				} else
					callback([], results.insertId)
			})
		},

		getAccountsByBandSeen: function (band, accountId, callback) {

			const query = 'SELECT DISTINCT a.id, a.username FROM accounts AS a, shows AS s WHERE a.id = s.accountId AND a.id != ? AND s.name = ?'
			const values = [accountId, band]

			db.query(query, values, function (error, accounts) {
				if (error)
					callback(['databaseError'], null)
				else
					callback([], accounts)
			})
		},

		getAccountsByShowSeen: function (show, accountId, callback) {

			show = show.replace(/,\s+/g, ',');
			res = show.split(",")

			const query = 'SELECT DISTINCT a.id, a.username FROM accounts AS a, shows AS s WHERE a.id = s.accountId AND a.id != ? AND s.name = ? AND s.location = ? AND s.date = ?'
			const values = [accountId, res[0], res[1], res[2]]

			db.query(query, values, function (error, accounts) {
				if (error)
					callback(['databaseError'], null)
				else
					callback([], accounts)
			})
		},

		getAccountById: function (id, callback) {

			const query = `SELECT * FROM accounts WHERE id = ? LIMIT 1`

			db.query(query, [id], function (error, accounts) {
				if (error)
					callback(['databaseError'], null)
				else
					callback([], accounts[0])
			})
		},
	}
}


