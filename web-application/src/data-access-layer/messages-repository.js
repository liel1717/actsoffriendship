const db = require('./db')

module.exports = function ({ }) {

    return {

        createMessage: function (senderUsername, receiverUsername, message, callback) {

            const date = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60 * 1000).toISOString().substr(0, 19).replace('T', ' ')
            const query = "INSERT INTO messages (senderUsername, receiverUsername, message, date) VALUES (?, ?, ?, ?)"
            const values = [senderUsername, receiverUsername, message, date]

            db.query(query, values, function (error) {
                if (error) {
                    callback(["databaseError"])
                } else {
                    callback([])
                }
            })
        },

        getAccountMessages: function (accountUsername, callback) {

            const query = "SELECT * FROM messages WHERE receiverUsername = ? OR senderUsername = ?"

            db.query(query, [accountUsername, accountUsername], function (error, messages) {
                if (error) {
                    callback(["databaseError"], null)
                } else {
                    callback([], messages)
                }
            })
        }
    }
}


