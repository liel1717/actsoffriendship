const express = require('express')
const jwt = require('jsonwebtoken')

module.exports = function ({ accountManager, showsManager }) {

	const router = express.Router()

	const jwtSecret = "aopsdfiajwf4w334dsI0"

	router.use(function (request, response, next) {

		try {
			const authorizationHeader = request.get("Authorization")
			const accessTokenString = authorizationHeader.substr("Bearer ".length)

			request.payload = jwt.verify(accessTokenString, jwtSecret)
		} catch (e) { }

		next()
	})

	router.get("/shows", function (request, response) {

		const accountId = request.query.accountId
		const payload = request.payload

		showsManager.getShowsByAccountId(null, payload, accountId, function (errors, shows) {
			if (errors[0] == "databaseError")
				response.status(500).end()
			else if (errors[0] == "invalid_client" || errors[0] == "notLoggedIn")
				response.status(401).json({ error: "invalid_client" })
			else
				response.status(200).json(shows)
		})
	})

	router.post("/shows", function (request, response) {

		const payload = request.payload
		const name = request.body.name
		const location = request.body.location
		const date = request.body.date

		showsManager.addShow(null, payload, name, location, date, function (errors) {
			if (errors[0] == "databaseError")
				response.status(500).end()
			else if (errors[0] == "invalid_client" || errors[0] == "notLoggedIn")
				response.status(401).json({ error: "invalid_client" })
			else
				response.status(201).end()
		})
	})

	router.delete("/shows/:id", function (request, response) {

		const id = request.params.id
		const payload = request.payload

		showsManager.deleteShow(id, payload, function (errors) {
			if (errors[0] == "databaseError")
				response.status(500).end()
			else if (errors[0] == "invalid_client")
				response.status(401).json({ error: "invalid_client" })
			else if (errors[0] == "deleted")
				response.status(204).end()
			else
				response.status(404).json({ error: "invalid_request" })
		})
	})

	router.put("/shows/:id", function (request, response) {

		const id = request.params.id
		const payload = request.payload
		const name = request.body.name
		const location = request.body.location
		const date = request.body.date

		showsManager.updateShow(id, payload, name, location, date, function (errors) {
			if (errors[0] == "databaseError")
				response.status(500).end()
			else if (errors[0] == "invalid_client")
				response.status(401).json({ error: "invalid_client" })
			else if (errors[0] == "updated")
				response.status(204).end()
			else
				response.status(404).json({ error: "invalid_request" })
		})
	})

	router.post("/accounts", function (request, response) {

		const username = request.body.username
		const password = request.body.password
		const password2 = request.body.password2

		accountManager.createAccount(username, password, password2, function (errors) {
			if (errors.length > 0)
				response.status(500).end()
			else
				response.status(201).end()
		})
	})

	router.post("/tokens", function (request, response) {

		const grant_type = request.body.grant_type
		const username = request.body.username
		const password = request.body.password

		accountManager.validateLogIn(grant_type, username, password, function (errors, account) {

			if (errors[0] == "databaseError")
				response.status(500).end()
			else if (errors[0] == "invalid_client")
				response.status(401).json({ error: "invalid_client" })
			else {
				const accessToken = jwt.sign({ accountId: account.id }, jwtSecret)
				const tokenType = "Bearer"
				const idToken = jwt.sign({ sub: account.id, preferred_username: account.username }, "kajsgydfaksydgfaksfgs")

				response.status(200).json({ access_token: accessToken, token_type: tokenType, id_token: idToken })
			}
		})
	})

	router.get("/shows/:id", function (request, response) {

		const id = request.params.id

		if (!request.payload) {
			response.status(401).end()
			return
		}

		showsManager.getShowById(id, request.payload, function (errors, show) {

			if (errors[0] == "databaseError")
				response.status(500).end()
			else if (errors[0] == "invalid_client")
				response.status(401).json({ error: "invalid_client" })
			else
				response.status(200).json(show)
		})
	})

	return router
}