const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const cors = require('cors')

module.exports = function ({ apiRouter }) {

	app.use(cors())

	app.use(bodyParser.urlencoded({
		extended: false
	}))

	app.use(bodyParser.json())

	app.use(apiRouter)

	return app
}

