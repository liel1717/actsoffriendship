const awilix = require('awilix')

const accountRepository = require('./data-access-layer/account-repository')
const friendsRepository = require('./data-access-layer/friends-repository')
const messagesRepository = require('./data-access-layer/messages-repository')
const showsRepository = require('./data-access-layer/shows-repository')
/*
const accountRepository = require('./data-access-layer-2/account-repository')
const friendsRepository = require('./data-access-layer-2/friends-repository')
const messagesRepository = require('./data-access-layer-2/messages-repository')
const showsRepository = require('./data-access-layer-2/shows-repository')
*/
const accountManager = require('./business-logic-layer/account-manager')
const accountValidator = require('./business-logic-layer/account-validator')
const friendsManager = require('./business-logic-layer/friends-manager')
const messagesManager = require('./business-logic-layer/messages-manager')
const showsManager = require('./business-logic-layer/shows-manager')
const showValidator = require('./business-logic-layer/show-validator')

const variousRouter = require('./presentation-layer/routers/various-router')
const accountRouter = require('./presentation-layer/routers/account-router')
const findfriendsRouter = require('./presentation-layer/routers/findfriends-router')
const apiRouter = require('./presentation-layer-2/routers/api-router')

const app = require('./presentation-layer/app')
const apiApp = require('./presentation-layer-2/api-app')

const container = awilix.createContainer()

container.register("accountRepository", awilix.asFunction(accountRepository))
container.register("friendsRepository", awilix.asFunction(friendsRepository))
container.register("messagesRepository", awilix.asFunction(messagesRepository))
container.register("showsRepository", awilix.asFunction(showsRepository))

container.register("accountManager", awilix.asFunction(accountManager))
container.register("accountValidator", awilix.asFunction(accountValidator))
container.register("friendsManager", awilix.asFunction(friendsManager))
container.register("messagesManager", awilix.asFunction(messagesManager))
container.register("showsManager", awilix.asFunction(showsManager))
container.register("showValidator", awilix.asFunction(showValidator))


container.register("variousRouter", awilix.asFunction(variousRouter))
container.register("accountRouter", awilix.asFunction(accountRouter))
container.register("findfriendsRouter", awilix.asFunction(findfriendsRouter))
container.register("apiRouter", awilix.asFunction(apiRouter))

container.register("app", awilix.asFunction(app))
container.register("apiApp", awilix.asFunction(apiApp))

const theApp = container.resolve("app")

theApp.listen(8080, function () {
    console.log('Running on 8080!')
})