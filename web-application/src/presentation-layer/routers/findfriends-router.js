const express = require('express')

module.exports = function ({ accountManager, friendsManager, showsManager }) {

    const router = express.Router()

    router.get("/", function (request, response) {
        const accountLoggedIn = request.session.accountLoggedIn
        showsManager.getUniqueShowNamesForAccount(accountLoggedIn, function (errors, shows) {

            if (errors[0] == "notLoggedIn") {
                response.redirect("/account/sign-in")
                return
            }
            const model = {
                accountLoggedIn: accountLoggedIn,
                shows: shows
            }

            response.render("findfriends-bands.hbs", model)
        })
    })

    router.get("/shows", function (request, response) {
        const accountLoggedIn = request.session.accountLoggedIn

        showsManager.getShowsByAccountId(accountLoggedIn, null, null, function (errors, shows) {

            if (errors[0] == "notLoggedIn") {
                response.redirect("/account/sign-in")
                return
            }

            const model = {
                accountLoggedIn: accountLoggedIn,
                shows: shows
            }

            response.render("findfriends-shows.hbs", model)
        })
    })

    router.post("/bandsearch", function (request, response) {

        const accountLoggedIn = request.session.accountLoggedIn
        const band = request.body.band
        const accountId = accountLoggedIn.id

        accountManager.getAccountsByBandSeen(band, accountId, function (errors, accounts) {
            const model = {
                band: band,
                accountLoggedIn: accountLoggedIn,
                errors: errors,
                accounts: accounts
            }
            response.render("findfriends-bands-result.hbs", model)
        })
    })

    router.post("/showsearch", function (request, response) {

        const accountLoggedIn = request.session.accountLoggedIn
        const show = request.body.show
        const accountId = accountLoggedIn.id

        accountManager.getAccountsByShowSeen(show, accountId, function (errors, accounts) {

            const model = {
                show: show,
                accountLoggedIn: accountLoggedIn,
                errors: errors,
                accounts: accounts
            }
            response.render("findfriends-shows-result.hbs", model)
        })
    })

    router.post("/addfriend/:id", function (request, response) {

        const accountLoggedIn = request.session.accountLoggedIn
        const id1 = accountLoggedIn.id
        const id2 = request.params.id
        const username = request.body.username

        friendsManager.addFriend(accountLoggedIn, id1, id2, function (errors) {

            if (errors[0] == "notLoggedIn") {
                response.redirect("/account/sign-in")
                return
            }

            const model = {
                username: username,
                accountLoggedIn: accountLoggedIn,
                errors: errors
            }
            response.render("findfriends-addfriend-result.hbs", model)
        })
    })

    return router
}

