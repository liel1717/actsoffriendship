const express = require('express')

module.exports = function ({ }) {

	const router = express.Router()

	router.get("/", function (request, response) {
		const model = { accountLoggedIn: request.session.accountLoggedIn }
		response.render("home.hbs", model)
	})

	return router
}