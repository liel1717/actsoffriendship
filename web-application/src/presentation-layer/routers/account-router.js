const express = require('express')

module.exports = function ({ accountManager, showsManager, friendsManager, messagesManager }) {

	const router = express.Router()

	router.get("/", function (request, response) {
		response.redirect("/account/shows")
	})

	router.get("/shows", function (request, response) {

		const accountLoggedIn = request.session.accountLoggedIn

		showsManager.getShowsByAccountId(accountLoggedIn, null, null, function (errors, shows) {

			if (errors[0] == "notLoggedIn") {
				response.redirect("/account/sign-in")
				return
			}

			const model = {
				accountLoggedIn: accountLoggedIn,
				errors: errors,
				shows: shows
			}

			response.render("account-shows.hbs", model)
		})
	})

	router.get('/messages', function (request, response) {

		const accountLoggedIn = request.session.accountLoggedIn

		messagesManager.getAccountMessages(accountLoggedIn, function (errors, messages) {

			if (errors[0] == "notLoggedIn") {
				response.redirect("/account/sign-in")
				return
			}

			const model = {
				accountLoggedIn: accountLoggedIn,
				errors: errors,
				messages: messages
			}
			response.render("account-messages.hbs", model)
		})
	})

	router.get("/sendMessage/:username", function (request, response) {

		const accountLoggedIn = request.session.accountLoggedIn
		const receiverUsername = request.params.username

		const model = {
			accountLoggedIn: accountLoggedIn,
			receiverUsername: receiverUsername
		}

		response.render("account-send-message.hbs", model)
	})

	router.post("/sendMessage/:username", function (request, response) {

		const accountLoggedIn = request.session.accountLoggedIn
		const receiverUsername = request.params.username
		const message = request.body.message

		messagesManager.createMessage(accountLoggedIn, receiverUsername, message, function (errors) {

			if (errors[0] == "notLoggedIn") {
				response.redirect("/account/sign-in")
				return
			} else if (errors.length > 0) {
				const model = {
					accountLoggedIn: accountLoggedIn,
					errors: errors,
					receiverUsername: receiverUsername
				}
				response.render("account-send-message.hbs", model)
			} else {
				response.redirect("/account/messages")
			}
		})
	})

	router.get('/friends', function (request, response) {

		const accountLoggedIn = request.session.accountLoggedIn

		friendsManager.getFriendsAccounts(accountLoggedIn, function (errors, friendsAccounts) {

			if (errors[0] == "notLoggedIn") {
				response.redirect("/account/sign-in")
				return
			}

			const model = {
				accountLoggedIn: accountLoggedIn,
				errors: errors,
				accounts: friendsAccounts
			}

			response.render("account-friends.hbs", model)
		})
	})

	router.get("/add-show", function (request, response) {

		const accountLoggedIn = request.session.accountLoggedIn
		const model = { accountLoggedIn: accountLoggedIn }

		response.render("account-add-show.hbs", model)
	})

	router.post("/add-show", function (request, response) {

		const accountLoggedIn = request.session.accountLoggedIn
		const name = request.body.name
		const location = request.body.location
		const date = request.body.date

		showsManager.addShow(accountLoggedIn, null, name, location, date, function (errors) {
			const model = {
				accountLoggedIn: accountLoggedIn,
				errors: errors
			}
			if (errors.length > 0)
				response.render("account-add-show.hbs", model)
			else
				response.redirect("/account")
		})
	})

	router.get("/sign-up", function (request, response) {
		const model = { accountLoggedIn: request.session.accountLoggedIn }
		response.render("account-sign-up.hbs", model)
	})

	router.post("/sign-up", function (request, response) {

		const accountLoggedIn = request.session.accountLoggedIn

		const username = request.body.username
		const password = request.body.password
		const password2 = request.body.password2

		accountManager.createAccount(username, password, password2, function (errors) {
			const model = {
				accountLoggedIn: accountLoggedIn,
				errors: errors
			}
			response.render("home.hbs", model)
		})
	})

	router.get("/sign-in", function (request, response) {
		const model = { accountLoggedIn: request.session.accountLoggedIn }
		response.render("account-sign-in.hbs", model)
	})

	router.post("/sign-in", function (request, response) {

		const logInInfo = {
			username: request.body.username,
			password: request.body.password
		}

		accountManager.signIn(logInInfo, function (errors, account) {

			if (errors.length == 0)
				request.session.accountLoggedIn = account

			const model = {
				accountLoggedIn: request.session.accountLoggedIn,
				errors: errors
			}
			response.render("home.hbs", model)
		})
	})

	router.post("/sign-out", function (request, response) {
		request.session.accountLoggedIn = false
		const model = { accountLoggedIn: request.session.accountLoggedIn }
		response.render("home.hbs", model)
	})

	return router
}

