const path = require('path')
const express = require('express')
const expressHandlebars = require('express-handlebars')
const bodyParser = require('body-parser')
const app = express()
const session = require('express-session')
const RedisStore = require('connect-redis')(session)

module.exports = function ({ variousRouter, accountRouter, findfriendsRouter, apiApp }) {

	app.use(session({
		store: new RedisStore({ host: "redis" }),
		saveUninitialized: false,
		secret: 'keyboard cat',
		resave: false
	}));

	app.use(bodyParser.urlencoded({
		extended: false
	}))

	app.set('views', path.join(__dirname, 'views'))

	app.engine('hbs', expressHandlebars({
		extname: 'hbs',
		defaultLayout: 'main',
		layoutsDir: path.join(__dirname, 'layouts')
	}))

	app.use(express.static(path.join(__dirname, 'public')))

	app.use('/', variousRouter)
	app.use('/account', accountRouter)
	app.use('/findfriends', findfriendsRouter)
	app.use('/api', apiApp)

	return app
}

