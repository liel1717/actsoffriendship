const models = require('./models')
const Sequelize = require('sequelize')

const messages = models.message

module.exports = function ({ }) {

    return {

        createMessage: function (senderUsername, receiverUsername, message, callback) {

            const date = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60 * 1000).toISOString().substr(0, 19).replace('T', ' ')

            messages.create({ senderUsername: senderUsername, receiverUsername: receiverUsername, message: message, date: date })
                .then(function (account) { callback([]) })
                .catch(function (error) { console.log(error); callback(["databaseError"]) })
        },

        getAccountMessages: function (accountUsername, callback) {

            messages.findAll({ where: { [Sequelize.Op.or]: [{ receiverUsername: accountUsername }, { senderUsername: accountUsername }] } })
                .then(function (messages) {
                    callback([], messages)
                })
                .catch(function (error) {
                    callback(["databaseError"], null)
                })
        }
    }
}