const Sequelize = require('sequelize')
const sequelize = require('./db')

const account = sequelize.define('account', {
    username: { type: Sequelize.TEXT, unique: true, allowNull: false },
    password: { type: Sequelize.TEXT, allowNull: false }
})

const show = sequelize.define('show', {
    name: { type: Sequelize.TEXT, allowNull: false },
    location: { type: Sequelize.TEXT, allowNull: false },
    date: { type: Sequelize.TEXT, allowNull: false }
})

const friend = sequelize.define('friend', {
})

const message = sequelize.define('message', {
    senderUsername: { type: Sequelize.TEXT, allowNull: false },
    receiverUsername: { type: Sequelize.TEXT, allowNull: false },
    message: { type: Sequelize.TEXT, allowNull: false },
    date: { type: Sequelize.TEXT, allowNull: false }
})

account.hasMany(show, { as: 'show' })
show.belongsTo(account)
account.hasMany(message, { as: 'message' })
friend.belongsTo(account, { as: 'friend1' })
friend.belongsTo(account, { as: 'friend2' })

sequelize.sync()

var a = 0

if (a == 0) {
    account.create({ username: "eddie", password: "$2a$08$8QGbsTzPB0Tgz7Uu5PMW9OJGy5LQJNR9XN.09E1eevLsRjulHF7kq" })
    account.create({ username: "alice", password: "$2a$08$8QGbsTzPB0Tgz7Uu5PMW9OJGy5LQJNR9XN.09E1eevLsRjulHF7kq" })
    account.create({ username: "kungsboa", password: "$2a$08$8QGbsTzPB0Tgz7Uu5PMW9OJGy5LQJNR9XN.09E1eevLsRjulHF7kq" })

    show.create({ accountId: 1, name: "Coldplay", location: "Roskilde", date: "2003-06-27" })
    show.create({ accountId: 1, name: "Iron Maiden", location: "Roskilde", date: "2003-06-29" })
    show.create({ accountId: 1, name: "Coldplay", location: "Glastonbury", date: "2005-06-15" })
    show.create({ accountId: 1, name: "Oasis", location: "Glastonbury", date: "2005-06-15" })
    show.create({ accountId: 1, name: "Blur", location: "York", date: "2002-01-03" })
    show.create({ accountId: 1, name: "Blur", location: "London", date: "2000-07-02" })

    show.create({ accountId: 2, name: "Coldplay", location: "Roskilde", date: "2003-06-27" })
    show.create({ accountId: 2, name: "Iron Maiden", location: "Stockholm", date: "2008-01-15" })
    show.create({ accountId: 2, name: "Iron Maiden", location: "New York", date: "2015-07-01" })
    show.create({ accountId: 2, name: "Oasis", location: "London", date: "2004-05-15" })
    show.create({ accountId: 2, name: "Refused", location: "Ryd", date: "1999-09-19" })

    show.create({ accountId: 3, name: "Coldplay", location: "Stockholm", date: "2011-07-23" })
    show.create({ accountId: 3, name: "Iron Maiden", location: "Stockholm", date: "2008-01-15" })
    show.create({ accountId: 3, name: "Coldplay", location: "Glastonbury", date: "2005-06-15" })
    show.create({ accountId: 3, name: "Oasis", location: "Stockholm", date: "2009-02-16" })
    show.create({ accountId: 3, name: "Refused", location: "Ryd", date: "2001-03-29" })
    show.create({ accountId: 3, name: "Eagles", location: "Los Angeles", date: "1976-07-07" })
    show.create({ accountId: 3, name: "Eagles", location: "San Francisco", date: "1979-06-28" })
    a = 1
}

exports.account = account
exports.show = show
exports.friend = friend
exports.message = message