const models = require('./models')

const shows = models.show

module.exports = function ({ }) {

    return {

        getShowsByAccountId: function (accountId, callback) {
            shows.findAll({ where: { accountId: accountId } })
                .then(function (shows) {
                    callback([], shows)
                })
                .catch(function (error) {
                    callback(['databaseError'], null)
                })
        },

        getShowById: function (id, callback) {
            shows.findOne({ where: { id: id } })
                .then(function (show) {
                    console.log("i REP: " + show)
                    callback([], show)
                })
                .catch(function (error) { callback(['databaseError'], null) })
        },

        getAllShows: function (callback) {
            shows.findAll()
                .then(function (shows) { callback([], shows) })
                .catch(function (error) { callback(['databaseError'], null) })
        },

        addShow: function (accountId, name, location, date, callback) {
            shows.create({ accountId: accountId, name: name, location: location, date: date })
                .then(function () { callback([]) })
                .catch(function (error) { callback(["databaseError"]) })
        },

        getUniqueShowNamesForAccount: function (accountId, callback) {
            shows.findAll({ where: { accountId: accountId } })
                .then(function (shows) {
                    const uniqueShowsNames = []
                    for (var i = 0; i < shows.length; i++) {
                        if (i == 0)
                            uniqueShowsNames.push(shows[i])
                        for (var j = 0; j < uniqueShowsNames.length; j++) {
                            if (uniqueShowsNames[j].name == shows[i].name)
                                break
                            if (j == uniqueShowsNames.length - 1)
                                uniqueShowsNames.push(shows[i])
                        }
                    }
                    callback([], uniqueShowsNames)
                })
                .catch(function (error) {
                    callback(['databaseError'], null)
                })
        },

        deleteShow: function (id, accountId, callback) {
            shows.destroy({ where: { id: id, accountId: accountId } })
                .then(function (rowDeleted) {
                    if (rowDeleted == 1)
                        callback(['deleted'])
                    else
                        callback([])
                })
                .catch(function (error) { callback(['databaseError']) })
        },

        updateShow: function (id, accountId, name, location, date, callback) {
            shows.update(
                { name: name, location: location, date: date },
                { where: { id: id, accountId: accountId } }
            )
                .then(function (rowUpdated) {
                    if (rowUpdated == 1)
                        callback(['updated'])
                    else
                        callback([])
                })
                .catch(function (error) { callback(['databaseError']) })
        }
    }
}