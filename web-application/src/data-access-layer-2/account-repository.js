const models = require('./models')
const Sequelize = require('sequelize')

const accounts = models.account
const shows = models.show

module.exports = function ({ }) {

	return {

		getAllAccounts: function (callback) {

			accounts.findAll()
				.then(function (account) { callback([], account) })
				.catch(function (error) { callback(['databaseError'], null) })
		},

		getAccountByUsername: function (username, callback) {

			accounts.findOne({ where: { username: username } })
				.then(function (account) { callback([], account) })
				.catch(function (error) { callback(["databaseError"], null) })
		},

		createAccount: function (username, password, callback) {
			accounts.create({ username: username, password: password })
				.then(function (account) { callback([]) })
				.catch(function (error) { callback(["databaseError"]) })
		},

		getAccountsByBandSeen: function (band, accountId, callback) {

			shows.findAll({
				where: { name: band, accountId: { [Sequelize.Op.ne]: accountId } }, attributes: [],
				include: [{ model: accounts, attributes: ["id", "username"] }]
			}).then(function (shows) {
				const accounts = shows.map(s => s.account)
				const uniqueAccounts = []
				for (var i = 0; i < accounts.length; i++) {
					if (i == 0)
						uniqueAccounts.push(accounts[i])
					for (var j = 0; j < uniqueAccounts.length; j++) {
						if (uniqueAccounts[j].username == accounts[i].username)
							break
						if (j == uniqueAccounts.length - 1)
							uniqueAccounts.push(accounts[i])
					}
				}
				callback([], uniqueAccounts)
			}).catch(function (error) {
				callback(["databaseError"], null)
			})
		},

		getAccountsByShowSeen: function (show, accountId, callback) {

			show = show.replace(/,\s+/g, ',');
			res = show.split(",")

			const name = res[0]
			const location = res[1]
			const date = res[2]

			shows.findAll({
				where: { name: name, location: location, date: date, accountId: { [Sequelize.Op.ne]: accountId } }, attributes: [],
				include: [{ model: accounts, attributes: ["id", "username"] }]
			}).then(function (shows) {
				const accounts = shows.map(s => s.account)
				const uniqueAccounts = []
				for (var i = 0; i < accounts.length; i++) {
					if (i == 0)
						uniqueAccounts.push(accounts[i])
					for (var j = 0; j < uniqueAccounts.length; j++) {
						if (uniqueAccounts[j].username == accounts[i].username)
							break
						if (j == uniqueAccounts.length - 1)
							uniqueAccounts.push(accounts[i])
					}
				}
				callback([], uniqueAccounts)
			}).catch(function (error) {
				callback(["databaseError"], null)
			})
		},

		getAccountById: function (id, callback) {
			accounts.findById(id)
				.then(function (account) { callback([], account) })
				.catch(function (error) { callback(['databaseError'], null) })
		}
	}
}


