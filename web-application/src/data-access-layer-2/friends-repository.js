const models = require('./models')
const Sequelize = require('sequelize')

const friends = models.friend

module.exports = function ({ }) {

    return {

        addFriend: function (id1, id2, callback) {

            friends.create({ friend1Id: id1, friend2Id: id2 })
                .then(function (friend) { callback([]) })
                .catch(function (error) { callback(["databaseError"]) })
        },

        getFriendsIds: function (id, callback) {

            const friendsIds = []

            friends.findAll({
                attributes: ["friend1Id", "friend2Id"],
                where: { [Sequelize.Op.or]: [{ friend1Id: id }, { friend2Id: id }] }
            })
                .then(function (ids) {
                    for (var i = 0; i < ids.length; i++) {
                        if (ids[i].friend1Id != id)
                            friendsIds.push(ids[i].friend1Id)
                        else
                            friendsIds.push(ids[i].friend2Id)
                    }
                    callback([], friendsIds)
                })
                .catch(function (error) {
                    callback(["databaseError"], null)
                })
        }
    }
}