function updateViewShowPage(showId) {

	const errorMessage = document.getElementById("errorMessage")

	fetch("http://" + HOST + "/api/shows/" + showId, {
		method: "GET",
		headers: {
			"Authorization": "Bearer " + accessToken
		},
	}).then(function (response) {
		return response.json()
	}).then(function (show) {

		document.querySelector("#view-show h1").innerText = show.name
		document.querySelector("#view-location").innerText = "Location: " + show.location
		document.querySelector("#view-date").innerText = "Date: " + show.date

		document.getElementById("edit-link").setAttribute("href", "/edit-show/" + show.id)
		document.getElementById("delete-button").setAttribute("href", "/delete-show/" + show.id)

	}).catch(function (error) {
		errorMessage.innerText = ""
		console.log(error)
		changePage("/error")
	})
}

document.addEventListener("DOMContentLoaded", function () {
	const a = document.getElementById("edit-link")
	a.addEventListener("click", handleClickOnAnchor)
})