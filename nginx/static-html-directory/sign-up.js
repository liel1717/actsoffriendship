document.addEventListener("DOMContentLoaded", function () {

	const signUpForm = document.querySelector("#sign-up form")

	signUpForm.addEventListener("submit", function (event) {
		event.preventDefault();

		const username = document.getElementById("sign-up-username").value
		const password = document.getElementById("sign-up-password").value
		const password2 = document.getElementById("sign-up-password2").value
		const errorMessage = document.getElementById("errorMessage")

		/*FOR EXAMINER: This validation exists in the function being used by the API but we 
		reason that we might aswell do this now so not to send a request that will surely fail*/
		if (username.length == 0 || password.length == 0 || password2.length == 0) {
			errorMessage.innerText = "Please enter a username and password twice"
			changePage("/error")
			return
		}
		if (password != password2) {
			errorMessage.innerText = "Passwords does not match"
			changePage("/error")
			return
		}
		
		fetch("http://" + HOST + "/api/accounts", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({ username: username, password: password, password2: password2 })
		}).then(function (response) {

			if (response.status == 500) {
				errorMessage.innerText = "internal server error"
				console.log(response)
				changePage("/error")
				return
			}
			changePage("/sign-in")
		}).catch(function (error) {
			errorMessage.innerText = ""
			console.log(error)
			changePage("/error")
		})
	})
})