function updateYourShows() {

	const errorMessage = document.getElementById("errorMessage")

	fetch("http://" + HOST + "/api/shows?accountId=" + userInfo.sub, {
		method: "GET",
		headers: {
			"Authorization": "Bearer " + accessToken
		},
	}).then(function (response) {
		return response.json()
	}).then(function (shows) {

		const ul = document.querySelector("#your-shows ul")
		ul.innerText = ""

		for (const show of shows) {
			const li = document.createElement("li")
			const a = document.createElement("a")

			a.innerText = show.name + ", " + show.location + ", " + show.date
			a.setAttribute("href", "/shows/" + show.id)
			a.addEventListener("click", handleClickOnAnchor)

			li.appendChild(a)
			ul.appendChild(li)
		}

	}).catch(function (error) {
		errorMessage.innerText = ""
		console.log(error)
		changePage("/error")
	})
}