document.addEventListener("DOMContentLoaded", function () {

	const anchors = document.querySelectorAll("a")

	for (const anchor of anchors) {
		anchor.addEventListener("click", handleClickOnAnchor)
	}

})

function handleClickOnAnchor(event) {
	event.preventDefault()
	const uri = event.currentTarget.getAttribute("href")
	changePage(uri)
	history.pushState({ uri: uri }, "", uri)
}

history.replaceState({ uri: "/" }, "", "/")

window.addEventListener('popstate', function (event) {
	const state = event.state
	changePage(state.uri)
})

function changePage(uri) {

	document.querySelector(".current-page").classList.remove("current-page")

	let id

	if (uri == "/") {
		id = "home"
	} else if (uri == "/sign-up") {
		id = "sign-up"
	} else if (uri == "/sign-in") {
		id = "sign-in"
	} else if (uri == "/your-shows") {
		id = "your-shows"
		updateYourShows()
	} else if (uri == "/add-show") {
		id = "add-show"
	} else if (uri.startsWith("/edit-show/")) {
		const showId = parseInt(uri.split("/")[2])
		id = "edit-show"
		updateEditShowPage(showId)
	} else if (uri.startsWith("/shows/")) {
		const showId = parseInt(uri.split("/")[2])
		id = "view-show"
		updateViewShowPage(showId)
	} else if (uri.startsWith("/delete-show/")) {
		const showId = parseInt(uri.split("/")[2])
		id = "edit-show"
		deleteShow(showId)
	} else if (uri == "/error") {
		id = "error"
	}

	document.getElementById(id).classList.add("current-page")
}