document.addEventListener("DOMContentLoaded", function () {

	const addShowForm = document.querySelector("#add-show form")

	addShowForm.addEventListener("submit", function (event) {
		event.preventDefault();

		const name = document.getElementById("name").value
		const location = document.getElementById("location").value
		const date = document.getElementById("date").value
		const errorMessage = document.getElementById("errorMessage")

		/*FOR EXAMINER: This validation exists in the function being used by the API but we 
		reason that we might aswell do this now so not to send a request that will surely fail*/
		if (name.length == 0 || location.length == 0 || date.length == 0) {
			errorMessage.innerText = "Please fill in all fields"
			changePage("/error")
			return
		}

		fetch("http://" + HOST + "/api/shows", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": "Bearer " + accessToken
			},
			body: JSON.stringify({ name: name, location: location, date, accountId: userInfo.sub })
		}).then(function (response) {
			changePage("/your-shows")
		}).catch(function (error) {
			errorMessage.innerText = ""
			console.log(error)
			changePage("/error")
		})
	})
})