function deleteShow(showId) {

	cachedShowId = showId
	const errorMessage = document.getElementById("errorMessage")

	fetch("http://" + HOST + "/api/shows/" + showId, {
		method: "DELETE",
		headers: {
			"Authorization": "Bearer " + accessToken
		},
	}).then(function (response) {
		changePage("/your-shows")
	}).catch(function (error) {
		errorMessage.innerText = ""
		console.log(error)
		changePage("/error")
	})
}