var userInfo
var accessToken

document.addEventListener("DOMContentLoaded", function () {

	const signInForm = document.querySelector("#sign-in form")

	signInForm.addEventListener("submit", function (event) {
		event.preventDefault();

		const username = document.getElementById("sign-in-username").value
		const password = document.getElementById("sign-in-password").value
		const errorMessage = document.getElementById("errorMessage")

		/*FOR EXAMINER: This validation exists in the function being used by the API but we 
		reason that we might aswell do this now so not to send a request that will surely fail*/
		if (username.length == 0 || password.length == 0) {
			errorMessage.innerText = "Please enter a username and password"
			changePage("/error")
			return
		}

		fetch("http://" + HOST + "/api/tokens", {
			method: "POST",
			headers: {
				"Content-Type": "application/x-www-form-urlencoded"
			},
			body: "grant_type=password&username=" + encodeURI(username) + "&password=" + encodeURI(password)
		}).then(function (response) {
			return response.json()
		}).then(function (body) {

			accessToken = body.access_token
			const idToken = body.id_token

			userInfo = jwt_decode(idToken)
			document.querySelector("nav").classList.add("user-is-logged-in")
			changePage("/your-shows")

		}).catch(function (error) {
			if (error == "InvalidTokenError: Invalid token specified")
				errorMessage.innerText = "Wrong password"
			else if (error == "SyntaxError: Unexpected end of JSON input")
				errorMessage.innerText = "Username does not exist"
			else
				errorMessage.innerText = ""
			console.log(error)
			changePage("/error")
		})
	})
})