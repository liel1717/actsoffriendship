let cachedShowId = null

function updateEditShowPage(showId) {

	cachedShowId = showId
	const errorMessage = document.getElementById("errorMessage")

	fetch("http://" + HOST + "/api/shows/" + showId, {
		method: "GET",
		headers: {
			"Authorization": "Bearer " + accessToken
		},
	}).then(function (response) {
		return response.json()
	}).then(function (show) {

		document.getElementById("edit-name").value = show.name
		document.getElementById("edit-location").value = show.location
		document.getElementById("edit-date").value = show.date

	}).catch(function (error) {
		errorMessage.innerText = ""
		console.log(error)
		changePage("/error")
	})
}

document.addEventListener("DOMContentLoaded", function () {

	const editShowForm = document.querySelector("#edit-show form")
	const errorMessage = document.getElementById("errorMessage")

	editShowForm.addEventListener("submit", function (event) {
		event.preventDefault();

		const name = document.getElementById("edit-name").value
		const location = document.getElementById("edit-location").value
		const date = document.getElementById("edit-date").value

		/*FOR EXAMINER: This validation exists in the function being used by the API but we 
		reason that we might aswell do this now so not to send a request that will surely fail*/
		if (name.length == 0 || location.length == 0 || date.length == 0) {
			errorMessage.innerText = "Please fill in all fields"
			changePage("/error")
			return
		}

		fetch("http://" + HOST + "/api/shows/" + cachedShowId, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": "Bearer " + accessToken
			},
			body: JSON.stringify({ id: cachedShowId, name: name, location: location, date: date, accountId: userInfo.sub })
		}).then(function (response) {
			console.log(response)
			changePage("/your-shows")
		}).catch(function (error) {
			errorMessage.innerText = ""
			console.log(error)
			changePage("/error")
		})

	})

})